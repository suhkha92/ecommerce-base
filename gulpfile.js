var gulp = require('gulp'),
stylus = require('gulp-stylus'),
autoprefixer = require('gulp-autoprefixer'),
minifyCSS	= require('gulp-minify-css'),
jade	= require('gulp-jade'),
uglify = require('gulp-uglify'),
rename = require('gulp-rename'),
concat = require('gulp-concat'),
connect = require('gulp-connect'); //Live-reload

//manage tasks
gulp.task('css', function(){
	gulp.src('resources/assets/stylus/landing.styl')
		//stylus compilation
		.pipe(stylus({error: true, compress: false, paths: ['resources/assets/stylus']}))
		//compile styles and add prefixing
		.pipe(autoprefixer())
		//clean prefixed CSS
		//.pipe(minifyCSS())
		.pipe(rename('landing.css'))
		.pipe(gulp.dest('public/css'))
		.pipe(connect.reload())
	});

gulp.task('vendor-css', function(){
	gulp.src([
		'bower_components/bootstrap/dist/css/bootstrap.min.css'
		])
	.pipe(concat('vendor.min.css'))
	.pipe(minifyCSS())
	.pipe(gulp.dest('public/css'))
});

gulp.task('js', function(){
	gulp.src('public/scripts/*.js')
		.pipe(connect.reload())
	});

gulp.task('html', function(){
	gulp.src('public/*.html')
		.pipe(connect.reload())
	});


gulp.task('plugins-js', function(){
	gulp.src([
		'bower_components/jquery/dist/jquery.js',
		'bower_components/modernizr/modernizr.js'
		])
	.pipe(concat('vendor.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('public/scripts'))
});



// Start Live reload server
// gulp.task('connect', function() {
// 	connect.server({
// 		root: 'public',
// 		livereload: true,
// 	});
// });


//task for watch our changes
gulp.task('watch', function(){
	gulp.watch('resources/assets/stylus/*.styl', ['css']);
	gulp.watch('public/scripts/*.js', ['js']);
});

//default
gulp.task('default', ['css','js','plugins-js','vendor-css','watch']);
