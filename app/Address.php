<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
		use updateGenericClass;
		
    protected $guarded = array();
    
    protected $table = "address";

    public static function getAllAddresses($id)
    {
    	$addresses = Address::where('user_id', '=', $id)
    						->get();
    	return $addresses;
    }
}
