<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Sell extends Model
{
		use updateGenericClass;
		
    protected $guarded = array();
	
    protected $table = "sells";

    public function sell()
    {
        return $this->belongsTo('App\User');
    }

    public static function getAllSells()
    {
    	$sells = DB::table('sells')
    	            ->join('users', 'users.id', '=', 'sells.user_id')
    	            ->select('users.name', 'sells.track_code', 'sells.email_status', 'sells.user_id', 'sells.id', 'sells.status', 'sells.total', 'sells.created_at')
    	            ->get();
    	 return $sells;
    }

    public static function stadisticSellByMonth()
    {
        $stadisticSellByMonth = DB::table('sells')
                    ->select(DB::raw('MONTH(created_at) as month'), DB::raw('YEAR(created_at) as year'), DB::raw('count(*) as count'))
                    ->groupBy(DB::raw('MONTH(created_at)'), DB::raw('YEAR(created_at)'))
                    ->get();
        return $stadisticSellByMonth;
    }

    public static function stadisticSellToday()
    {
        $stadisticSellToday = DB::table('sells')
                    ->select(DB::raw('COUNT(*) as count'))
                    ->where(DB::raw('DATE(created_at)'), '=', DB::raw('CURDATE()') )
                    ->count();
        return $stadisticSellToday;
    }

    public static function stadisticSellTodayMoney()
    {
        $stadisticSellTodayMoney = DB::table('sells')
                    ->select(DB::raw('SUM(total) as total'))
                    ->where(DB::raw('DATE(created_at)'), '=', DB::raw('CURDATE()') )
                    ->first();
        return $stadisticSellTodayMoney;
    }

    public static function stadisticSellTotalMoney()
    {
        $stadisticSellTotalMoney = DB::table('sells')
                    ->select(DB::raw('SUM(total) as total'))
                    ->first();
        return $stadisticSellTotalMoney;
    }

    public static function getTrackDataCustomer($id)
    {
        $track = DB::table('sells')
                    ->join('users', 'users.id', '=', 'sells.user_id')
                    ->select('users.name', 'sells.id', 'sells.lang', 'sells.track_code', 'users.email')
                    ->where('sells.user_id', '=', $id)
                    ->first();
        return $track;
    }

    public static function updateStatusTrackingEmail($id_sell)
    {
        Sell::where('id', $id_sell)
                ->update(['email_status' => '1']);
    }
}
