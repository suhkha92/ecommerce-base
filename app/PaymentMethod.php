<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
		use updateGenericClass;
		
    protected $guarded = array();
    
    protected $table = "payment_methods";
}
