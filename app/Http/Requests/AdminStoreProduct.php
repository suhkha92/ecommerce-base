<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminStoreProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'required',
            'name' => 'required|max:100',  
            'description' => 'required',
            'description_english' => 'required',
            'stock' => 'required|numeric',
            'price_mxn' => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'price_usd' => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'status' => 'required' 
        ];
    }
}
