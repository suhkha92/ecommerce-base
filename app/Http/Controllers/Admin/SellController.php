<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Sell;
use App\User;

class SellController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $sells = Sell::getAllSells();
        return view('admin.sells.index')
                ->with('sells', $sells);
    }
    public function edit($id)
    {   
        $sell = Sell::find($id);
        return view('admin.sells.edit')
                ->with('sell', $sell);
    }

    public function update(Request $request, $id)
    {
        Sell::updateData($id);
        return redirect()
                ->route('admin.sells.index')
                ->with('success', 'Guía de envío actualizada correctamente.');
    }

    public function status(Request $request)
    {
        $id = $request->input('id');
        $status = $request->input('status');
        $sell = Sell::find($id);
        if ($sell != "") 
        {
            \DB::table('sells')
            ->where('id', $id)
            ->update(['status' => $status]);
        }
        return redirect()
                ->route('admin.sells.index')
                ->with('success', 'Estatus actualizado correctamente.');
    }
}
