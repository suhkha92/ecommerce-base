<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminStoreCategory;
use App\Category;
use App\Product;


class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $categories = Category::all();
        return view('admin.categories.index', compact('categories', $categories));
    }

    public function create()
    {
        return view('admin.categories.new');
    }

    public function store(AdminStoreCategory $request)
    {
        $category = Category::create($request->all());
        return redirect()
                ->route('admin.categories.index')
                ->with('success', 'Categoría guardada correctamente.');
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return view('admin.categories.edit')
                ->with('category', $category);
    }

    public function update(Request $request, $id)
    {
        Category::updateData($id);
        return redirect()
                ->route('admin.categories.index')
                ->with('success', 'Categoría actualizada correctamente.');
    }

    public function delete($id)
    {
        $category = Category::find($id);
        if ($category->categoriesRelatedToProducts()->count()) {
           return redirect()
                   ->back()
                   ->with('error','No puedes borrar esta categoría. Tienes productos relacionados.');  
        }
        $category->delete();
        return redirect()
                ->route('admin.categories.index')
                ->with('success', 'Categoría eliminada correctamente.');
    }

    public function status(Request $request)
    {
        $id = $request->input('id');
        $category = Category::find($id);
        if ($category != "") 
        {
            $category->status = $category->status ? 0 : 1;
            $category->save();
        }
        return redirect()
                ->route('admin.categories.index')
                ->with('success', 'Categoría actualizada correctamente.');
    }
}
