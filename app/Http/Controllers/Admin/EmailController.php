<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Sell;
use App\User;
use App\StoreConfig;

class EmailController extends Controller
{
    public function sendTrackSell($id){
    	$track = Sell::getTrackDataCustomer($id);
        $store = StoreConfig::all()->first();
        $id_sell = $track->id;

        $data=[
            'track_code'=>$track->track_code,
            'name'=>$track->name,
            'lang'=>$track->lang,
            'facebook'=>$store->facebook,
            'twitter' => $store->twitter, 
            'instagram' => $store->instagram
        ];

        Mail::toto($track->email, $track->name)
            ->send(new UserNotificationShippingGuide($data));

    	Sell::updateStatusTrackingEmail($id_sell);

        return redirect()
                ->route('admin.sells.index')
                ->with('success', 'Guía enviada.');
    }
}
