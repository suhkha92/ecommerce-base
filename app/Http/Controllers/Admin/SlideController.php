<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminStoreSlide;
use App\Slide;

class SlideController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $slides = Slide::all();
        return view('admin.slides.index', compact('slides', $slides));
    }

    public function create()
    {
        return view('admin.slides.new');
    }

    public function store(AdminStoreSlide $request)
    {
        $slide = Slide::create($request->except('photo'));
        $id = $slide->id;      
        $path = $request->file('photo')
                        ->store('public');
        Slide::updatePicture($id, $path);
        return redirect()->route('admin.slides.index')
                        ->with('success', 'Slide guardado correctamente.');
    }

    public function edit($id)
    {
        $slide = Slide::find($id);
        return view('admin.slides.edit')
                ->with('slide', $slide);
    }

    public function update(Request $request, $id)
    {
        Slide::updateDataNoPicture($id);
        // Check if there is a photo in request
        if ($request->hasFile('photo')) 
        {
            // First delete old photo
            $slide = Slide::find($id);
            $currentPath = $slide['photo'];
            $deletedFile = \Storage::delete($currentPath);
            // Then save the new photo from request
            $path = $request->file('photo')->store('public');
            Slide::updatePicture($id, $path);
        } 
        return redirect()
                ->route('admin.slides.index')
                ->with('success', 'Slide actualizado correctamente.');
    }

    public function delete($id)
    {
        $slide = Slide::find($id);
        $path = $slide['photo'];
        \Storage::delete($path);
        $slide->delete();
        return redirect()
                ->route('admin.slides.index')
                ->with('success', 'Slide eliminado correctamente.');
    }

    public function status(Request $request)
    {
        $id = $request->input('id');
        $slide = Slide::find($id);
        if ($slide != "") 
        {
            $slide->status = $slide->status ? 0 : 1;
            $slide->save();
        }
        return redirect()
                ->route('admin.slides.index')
                ->with('success', 'Slide actualizado correctamente.');
    }
}
