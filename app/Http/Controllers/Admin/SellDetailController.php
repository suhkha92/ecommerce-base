<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SellDetail;

class SellDetailController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function show($id)
    {
        $sells = SellDetail::getSellDetails($id);
        for($i = 0; $i < count($sells); $i++){
        $sellsArr[] = array("id" => $sells[$i]->id,"sell_id" => $sells[$i]->sell_id, "product_id"=>$sells[$i]->product_id, "product"=>$sells[$i]->name, "price" => $sells[$i]->unit_price, "quantity" => $sells[$i]->quantity, "created_at" => $sells[$i]->created_at);
		}		
        return view('admin.sells.show')
                ->with('sellsArr', $sellsArr);
    }
}
