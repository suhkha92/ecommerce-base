<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\StoreConfig;

class StoreConfigController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function edit($id)
    {
        $storeConfig = StoreConfig::find($id);
        return view('admin.store-config.edit')
                ->with('store', $storeConfig);
    }

    public function update(Request $request, $id)
    {
        StoreConfig::updateDataNoPicture($id);
        // Check if there is a logo in request
        if ($request->hasFile('photo')) 
        {
            // First delete old logo
            $storeConfig = StoreConfig::find($id);
            $currentPath = $storeConfig['photo'];
            $deletedFile = \Storage::delete($currentPath);
            // Then save the new logo from request
            $path = $request->file('photo')->store('public');
            StoreConfig::updatePicture($id, $path);
        } 
        return redirect()
                ->route('admin.store-config.edit', ['id' => $id])
                ->with('success', 'Datos de la tienda actualizados correctamente.');
    }
}
