<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Lava;
use DB;
use App\SellDetail;
use App\Sell;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Display info about sells
        $stadisticProductsSold      =   SellDetail::stadisticProductsSold();
        $stadisticSellByMonth       =   Sell::stadisticSellByMonth();
        $stadisticSellToday         =   Sell::stadisticSellToday();
        $stadisticSellTodayMoney    =   Sell::stadisticSellTodayMoney();
        $stadisticSellTotalMoney    =   Sell::stadisticSellTotalMoney();
       
        //Graphics
        $products_sold  = Lava::DataTable();
        $products_sold->addStringColumn('Food Poll')
              ->addNumberColumn('Productos');
        for ($i=0; $i < count($stadisticProductsSold); $i++) { 
            $products_sold->addRow(array($stadisticProductsSold[$i]->name, $stadisticProductsSold[$i]->count));
        }

        $products['products'] = Lava::PieChart('Vendidos', $products_sold, [
            'title'  => ''
        ]);

        //Graphics
        $sell_month  = Lava::DataTable();
        $sell_month->addStringColumn('Food Poll')
              ->addNumberColumn('Ventas');
        for ($i=0; $i < count($stadisticSellByMonth); $i++) { 
            switch ($stadisticSellByMonth[$i]->month) {
                case '1':
                    $month = "Enero \n".$stadisticSellByMonth[$i]->year;
                    break;
                
                case '2':
                    $month = "Febrero \n".$stadisticSellByMonth[$i]->year;
                    break;

                case '3':
                    $month = "Marzo \n".$stadisticSellByMonth[$i]->year;
                    break;

                case '4':
                    $month = "Abril \n".$stadisticSellByMonth[$i]->year;
                    break;

                case '5':
                    $month = "Mayo \n".$stadisticSellByMonth[$i]->year;
                    break;

                case '6':
                    $month = "Junio \n".$stadisticSellByMonth[$i]->year;
                    break;

                case '7':
                    $month = "Julio \n".$stadisticSellByMonth[$i]->year;
                    break;

                case '8':
                    $month = "Agosto \n".$stadisticSellByMonth[$i]->year;
                    break;

                case '9':
                    $month = "Septiembre \n".$stadisticSellByMonth[$i]->year;
                    break;

                case '10':
                    $month = "Octubre \n".$stadisticSellByMonth[$i]->year;
                    break;

                case '11':
                    $month = "Noviembre \n".$stadisticSellByMonth[$i]->year;
                    break;

                case '12':
                    $month = "Diciembre \n".$stadisticSellByMonth[$i]->year;
                    break;
            }   

            $sell_month->addRow(array($month, $stadisticSellByMonth[$i]->count));
        }

        $sells['sells'] = Lava::ColumnChart('Ventas', $sell_month, [
            'title'  => '',
            'titleTextStyle' => [
                'color'    => '#eb6b2c',
                'fontSize' => 14
            ]
        ]);
        
        //Fact sell today

        return view('admin.dashboard')
                ->with($products)
                ->with($sells)
                ->with('sells_today', $stadisticSellToday)
                ->with('sells_today_money', $stadisticSellTodayMoney->total)
                ->with('sells_total_money', $stadisticSellTotalMoney->total);
    }
}
