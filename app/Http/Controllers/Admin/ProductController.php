<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminStoreProduct;
use App\Product;
use App\Category;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin'); 
    }

    public function getCategories()
    {
        return $categories = Category::all();
    }

    public function index()
    {
        $products = Product::all();
        return view('admin.products.index', compact('products', $products));
    }

    public function create()
    {
        $categories = $this->getCategories();
        return view('admin.products.new')
                ->with('categories', $categories);
    }

    public function store(AdminStoreProduct $request)
    {
        $product = Product::create($request->all());
        return redirect()
                ->route('admin.products.index')
                ->with('success', 'Producto guardado correctamente.');
    }

    public function show($id)
    {
        $product = Product::find($id);
        return view('admin.products.show')
                ->with('product', $product);
    }

    public function edit($id)
    {   
        $product = Product::find($id);
        $categories = $this->getCategories();
        return view('admin.products.edit')
                ->with('product', $product)
                ->with('categories', $categories);
    }

    public function update(Request $request, $id)
    {
        Product::updateData($id);
        return redirect()
                ->route('admin.products.index')
                ->with('success', 'Producto actualizado correctamente.');
    }

    public function delete($id)
    {
        $product = Product::find($id);
        $product->delete();
        return redirect()
                ->route('admin.products.index')
                ->with('success', 'Producto eliminado correctamente.');
    }

    public function status(Request $request)
    {
        $id = $request->input('id');
        $product = Product::find($id);
        if ($product != "") 
        {
            $product->status = $product->status ? 0 : 1;
            $product->save();
        }
        return redirect()
                ->route('admin.products.index')
                ->with('success', 'Producto actualizado correctamente.');
    }

    public function discount(Request $request)
    {
        $id = $request->input('id');
        $product = Product::find($id);
        if ($product != "") 
        {
            $product->discount = $product->discount ? 0 : 1;
            $product->save();
        }
        return redirect()
                ->route('admin.products.index')
                ->with('success', 'Producto actualizado correctamente.');
    }
}
