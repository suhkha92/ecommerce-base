<?php

namespace App;
use Illuminate\Database\Eloquent\Model;


class Category extends Model
{
		use updateGenericClass;

    protected $guarded = array();
    
    protected $table = "categories";

    public function categoriesRelatedToProducts()
    {
        return $this->hasMany('App\Product');
    }
}
