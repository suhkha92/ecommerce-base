<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use updateGenericClass;
    
	protected $guarded = array();
	
    protected $table = "products";

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function gallery()
    {
        return $this->hasMany('App\Gallery');
    }

    public function video()
    {
        return $this->hasMany('App\Video');
    }
}
