<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingMethod extends Model
{
		use updateGenericClass;
		
    protected $guarded = array();
    
    protected $table = "shipping_methods";
}
