<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreConfig extends Model
{
		use updateGenericClass;
		
    protected $guarded = array();
	
    protected $table = "store_config";
}
