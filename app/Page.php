<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
		use updateGenericClass;
		
    protected $guarded = array();
	
    protected $table = "pages";

    public static function showPage($slug)
    {
    	$page = Page::where('slug', '=', $slug)
    				->orWhere('slug_english', $slug)->first();
    	return $page;
    }

}
