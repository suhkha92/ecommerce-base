<?php  

namespace App;

use Exception;
use Illuminate\Http\Request;

    trait updateGenericClass {
    
    	public static function updateData($id)
      {
      	return self::where('id', $id)
      						 ->update(request()->except('_token'));
      }

      public static function updateDataNoPicture($id)
      {
      	return self::where('id', $id)
      	        		->update(request()->except('_token', 'photo'));
      }

      public static function updatePicture($id, $path)
      {
        return self::where('id', $id)
                   ->update(array('photo' => $path));
      }
    }

?>