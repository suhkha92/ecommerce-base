<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Video extends Model
{
    use updateGenericClass;
    
    protected $guarded = array();
    
    protected $table = "videos";

    public function product()
    {
    	return $this->belongsTo('App\Product')->withDefault(["name" => "Video general"]

    		);

    }
}
