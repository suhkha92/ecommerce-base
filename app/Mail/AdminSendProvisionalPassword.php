<?php

namespace App\Mail;

use App\User;
use App\StoreConfig;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminSendProvisionalPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $user;

    public function __construct(User $user)
    {
        //
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $store = StoreConfig::all()->first();
        return $this->view('admin.emails.provisional-password')
                        ->with('facebook', $store->facebook)
                        ->with('twitter', $store->twitter)
                        ->with('instagram', $store->instagram);
    }
}
