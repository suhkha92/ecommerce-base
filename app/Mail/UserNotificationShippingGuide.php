<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\StoreConfig;

class UserNotificationShippingGuide extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data;

    public function __construct(Array $data)
    {
        $this->data=$data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

      if($this->data['lang'] == 'es'){
          $view = 'admin.emails.shipping-guide';
          $subject = 'Nari, Tu pedido ha sido enviado';
      }else{
          $view = 'admin.emails.shipping-guide-en';
          $subject = 'Nari, Your order has been sent';
      }

      return $this->view($view)
              ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
              ->subject($subject)
              ->with([
                  'track_code' => $this->data['track_code'],
                  'name' => $this->data['name'],
                  'facebook' => $this->data['facebook'],
                  'twitter' => $this->data['twitter'],
                  'instagram' => $this->data['instagram']
                ]);
    }
}
