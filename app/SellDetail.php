<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class SellDetail extends Model
{
    protected $guarded = array();
	
    protected $table = "sells_details";

    
    public static function getSellDetails($id)
    {
    	$sells = DB::table('sells_details')
    	            ->join('sells', 'sells.id', '=', 'sells_details.sell_id')
    	            ->join('products', 'products.id', '=', 'sells_details.product_id')
    	            ->select('products.name', 'sells_details.product_id', 'sells_details.sell_id', 'sells_details.id', 'sells_details.unit_price', 'sells_details.quantity','sells_details.created_at')
    	            ->where('sell_id', $id)
    	            ->get();
    	  return $sells;
    }

    public static function stadisticProductsSold()
    {
        $stadisticProductsSold = DB::table('sells_details')
                    ->join('products', 'products.id', '=', 'sells_details.product_id')
                    ->select('name', DB::raw('count(*) as count'))
                    ->groupBy('product_id')
                    ->get();
        return $stadisticProductsSold;
    }
}
