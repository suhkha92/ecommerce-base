<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')
                  ->references('id')
                  ->on('categories');
            $table->string('name');
            $table->text('description');
            $table->integer('stock');
            $table->text('description_english');
            $table->float('price_mxn', 8, 2);
            $table->float('price_usd', 8, 2);
            $table->float('percentage_mxn', 8, 2);
            $table->float('percentage_usd', 8, 2);
            $table->string('discount');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
