<?php

use Illuminate\Database\Seeder;
use App\SellDetail;
use Faker\Factory as Faker;

class SellDetailTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for ($i=1; $i < 4; $i++) {
            $sell_detail = new SellDetail();
            $sell_detail->sell_id = $faker->numberBetween($min = 1, $max = 3);
            $sell_detail->product_id = $faker->numberBetween($min = 1, $max = 4);
            $sell_detail->unit_price = $faker->randomNumber(2);
            $sell_detail->quantity = $faker->numberBetween($min = 1, $max = 4);
            $sell_detail->save();
        }
    }
}
