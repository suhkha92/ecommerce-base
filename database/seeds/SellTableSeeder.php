<?php

use Illuminate\Database\Seeder;
use App\Sell;
use Faker\Factory as Faker;

class SellTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for ($i=1; $i < 4; $i++) {
            $sell = new Sell();
            $sell->user_id = $faker->numberBetween($min = 1, $max = 3);
            $sell->status = $faker->numberBetween($min = 1, $max = 5);
            $sell->total = $faker->randomNumber(2);
            $sell->lang = $faker->randomElement($array = array ('en','es'));
            $sell->save();
        }
    }
}
