<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(AdminsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(AddressesTableSeeder::class);
        $this->call(StoreConfigTableSeeder::class);
        $this->call(SellTableSeeder::class);
        $this->call(SellDetailTableSeeder::class);
    }
}
