<?php

use Illuminate\Database\Seeder;
use App\StoreConfig;
class StoreConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$storeConfig = new StoreConfig();
    	$storeConfig->name = 'Ecommerce';
    	$storeConfig->photo = 'logo-ecommerce.png';
    	$storeConfig->facebook = '#';
    	$storeConfig->twitter = '#';
    	$storeConfig->instagram = '#';
    	$storeConfig->contact_email = 'mail@mail.com';
    	$storeConfig->phone = '0000000000';

    	$storeConfig->save();
    }
}
