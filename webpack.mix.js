const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
//  if ( ! mix.inProduction()) {

// mix
//   .copy(
//   'node_modules/owl.carousel/dist/owl.carousel.js',
//   'resources/assets/vendor/owl.carousel/js')
//   .copy(
//   'node_modules/owl.carousel/dist/assets/owl.carousel.css',
//   'resources/assets/vendor/owl.carousel/css')
//   .copy(
//   'node_modules/moment/moment.js',
//   'public/js/moment.js')
// }

//mix
   // .scripts([ //default js
   //   'resources/assets/js/classie.js',
   //   'resources/assets/js/default.js',
   //   'resources/assets/js/modals.js',
   // ], 'public/js/default.js')

   mix.stylus('resources/assets/stylus/landing.styl', 'public/css')
   //.stylus('resources/assets/stylus/errors.styl', 'public/css');

if (mix.inProduction()) {
  mix.version();
}
