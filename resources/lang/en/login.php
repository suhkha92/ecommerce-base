<?php 

return [
    'title_login_form' => 'Login',
    'email_login_form' => 'E-mail Address',
    'password_login_form' => 'Password',
    'btn_login_form' => 'Login',
    'link_forgot_password_login_form' => 'Forgot Your Password?',
];
