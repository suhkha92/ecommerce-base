<?php 

return [
    'title_login_form' => 'Iniciar Sesión',
    'email_login_form' => 'Email',
    'password_login_form' => 'Contraseña',
    'btn_login_form' => 'Ingresa',
    'link_forgot_password_login_form' => '¿Olvidaste tu contraseña?',
];
