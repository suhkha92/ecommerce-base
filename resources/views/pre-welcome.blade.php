<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height" style="display: none;">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif

            <div class="container" style="display: none;">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="panel">
                           
                            <form action="changelocale" class="form-inline" method="post">
                            {{ csrf_field() }}
                               <div class="form-group @if($errors->first('locale')) has-error @endif">
                                   <span aria-hidden="true"><i class="fa fa-flag"></i></span>
                                    Select language please/Seleccionar idioma por favor
                                   <select name="locale" id="locale" onchange="this.form.submit()">
                                       <option value="">{{\App::getLocale()}}</option>
                                       <option value="es">ES</option>
                                       <option value="en">EN</option>
                                   </select>
                                   <small class="text-danger">{{ $errors->first('locale') }}</small>
                               </div>
                           </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content">
                <div class="title m-b-md">
                    Are you curious?
                </div>

                <div class="links">
                    <a href="#">Facebook</a>
                    <a href="#">Twitter</a>
                    <a href="#">Instagram</a>
                </div>
            </div>
    </body>
</html>
