@extends('layouts.inner--layout-admin')
@section('title-section-admin')Slides @stop

@section('content-admin')
	
	@if (session('success'))
		<div class="alert alert-dismissible alert-info">
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		  {{ session('success') }}
		</div>
	@endif
	@if (session('error'))
		<div class="alert alert-dismissible alert-danger">
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		  <strong>{{ session('error') }}</strong>
		</div>
	@endif
	<div class="table-responsive">
		<table class="table table-responsive table-striped table-hover" id="myTable">
			<thead>
				<tr>
					<th>#</th>
					<th>Nombre</th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			@if(count($slides) > 0)
				@foreach($slides as $slide)
					<tr>
						<td>{{$slide->id}}</td>
						<td>{{$slide->name}}</td>
						<td><a href="{{url('/admin/slides/edit/'.$slide->id)}}">Editar</a></td>
						<td>
							<form method="post" action="{{ url('/admin/slides/delete/'.$slide->id) }}">
								{{ csrf_field() }}
								<a href="" class="delete-link" data-toggle="modal", data-target="#delete__confirm"  data-title="Eliminar slide" data-message="¿Desea eliminar este slide?" data-btncancel="btn-default" data-btnaction="btn-danger" data-btntxt="Disable">Eliminar</a>
							</form>
							@include('includes.admin-modal-confirm-delete')
						</td>
					</tr>
				@endforeach
			@else
				No hay resultados
			@endif
			</tbody>
		</table> 
		
	</div>
	
@endsection