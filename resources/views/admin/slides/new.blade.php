@extends('layouts.inner--layout-admin')
@section('title-section-admin')Nuevo slide @stop

@section('content-admin')
	
	<form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{ route('admin.slides.store') }}">
		{{ csrf_field() }}
		<fieldset>
			<div class="form-group">
				<label for="inputName" class="col-lg-2 control-label">Nombre</label>
				<div class="col-lg-3">
					<input type="text" class="form-control" id="inputName" name="name" placeholder="Nombre de slide" value="{{ old('name') }}">
					@if ($errors->has('name'))
					    <span class="help-block">
					        <strong>{{ $errors->first('name') }}</strong>
					    </span>
					@endif
				</div>
				<label for="inputPhoto" class="col-lg-2 control-label">Foto</label>
				<div class="col-lg-3">
					<input type="file" name="photo" placeholder="Foto del slide">
					@if ($errors->has('photo'))
					    <span class="help-block">
					        <strong>{{ $errors->first('photo') }}</strong>
					    </span>
					@endif
				</div>
			</div>
			<div class="form-group">
				<label for="inputBio" class="col-lg-2 control-label">Descripción</label>
				<div class="col-lg-9">
					<textarea class="form-control" id="inputBio" name="description" placeholder="Descripción">{{ old('description') }}</textarea>
					@if ($errors->has('description'))
					    <span class="help-block">
					        <strong>{{ $errors->first('description') }}</strong>
					    </span>
					@endif
				</div>
			</div>
			<div class="form-group">
				<label for="inputBioEnglish" class="col-lg-2 control-label">Descripción en inglés</label>
				<div class="col-lg-9">
					<textarea class="form-control" id="inputBioEnglish" name="description_english" placeholder="Descripción en inglés">{{ old('description_english') }}</textarea>
					@if ($errors->has('description_english'))
					    <span class="help-block">
					        <strong>{{ $errors->first('description_english') }}</strong>
					    </span>
					@endif
				</div>
			</div>
			<div class="form-group">
				<label for="inputStatus" class="col-lg-2 control-label">Estatus</label>
				<div class="col-lg-3">
					<select name="status" class="form-control">
						<option value="">Seleccionar</option>
						<option value="1">Sí</option>
						<option value="0">No</option>
					</select>
					@if ($errors->has('status'))
					    <span class="help-block">
					        <strong>{{ $errors->first('status') }}</strong>
					    </span>
					@endif
				</div>
			</div>
			<button type="submit" class="btn btn-default col-lg-offset-11">Guardar</button>
		</fieldset>
	</form>
	
@endsection