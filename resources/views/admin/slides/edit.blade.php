@extends('layouts.inner--layout-admin')
@section('title-section-admin')Editar slide <a href="javascript:history.back()" class="right"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Regresar</a>@stop

@section('content-admin')

	<form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{ url('admin/slides/update/'.$slide->id) }}">
		{{ csrf_field() }}
		<fieldset>
			<div class="form-group">
				<label for="inputName" class="col-lg-2 control-label">Nombre</label>
				<div class="col-lg-3">
					<input type="text" class="form-control" id="inputName" name="name" placeholder="Nombre de slide" value="{{$slide->name}}">
					@if ($errors->has('name'))
					    <span class="help-block">
					        <strong>{{ $errors->first('name') }}</strong>
					    </span>
					@endif
				</div>
				<label for="inputPhoto" class="col-lg-2 control-label">Foto</label>
				<div class="col-lg-3">
					<img src="{{ Storage::url($slide->photo) }}" class="img-responsive img-thumbnail" width="150">
					<p>
						<small>{{$slide->photo}}</small>
					</p>
					<div>
					  <label for="photo" class="btn btn-primary">
					  Seleccionar nueva foto</label>
					  <input id="photo" type="file" style="visibility:hidden;" name="photo">
					</div>
					@if ($errors->has('photo'))
					    <span class="help-block">
					        <strong>{{ $errors->first('photo') }}</strong>
					    </span>
					@endif
				</div>
			</div>
			<div class="form-group">
				<label for="inputDescription" class="col-lg-2 control-label">Descripción</label>
				<div class="col-lg-9">
					<textarea class="form-control" id="inputDescription" name="description" placeholder="Descripción">
						{{$slide->description}}
					</textarea>
					@if ($errors->has('description'))
					    <span class="help-block">
					        <strong>{{ $errors->first('description') }}</strong>
					    </span>
					@endif
				</div>
			</div>
			<div class="form-group">
				<label for="inputDescriptionEnglish" class="col-lg-2 control-label">Descripción en inglés</label>
				<div class="col-lg-9">
					<textarea class="form-control" id="inputDescriptionEnglish" name="description_english" placeholder="Descripción en inglés">
						{{$slide->description_english}}
					</textarea>
					@if ($errors->has('description_english'))
					    <span class="help-block">
					        <strong>{{ $errors->first('description_english') }}</strong>
					    </span>
					@endif
				</div>
			</div>
			<button type="submit" class="btn btn-default col-lg-offset-11">Guardar</button>
		</fieldset>
	</form>
	
@endsection