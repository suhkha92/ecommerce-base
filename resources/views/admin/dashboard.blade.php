@extends('layouts.inner--layout-admin')
@section('title-section-admin')Dashboard <a href="{{url('/')}}" target="_blank" class="right"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Ir al sitio</a>@stop

@section('content-admin')
<div class="row">
		<div class="alert alert-dismissible alert-info col-md-4 data-dashboard--first">
		  <strong>Ventas del día: {{$sells_today}}</strong> 
		</div>

		<div class="alert alert-dismissible alert-info col-md-4">
		  <strong>Ventas del día: ${{number_format($sells_today_money, 2)}}</strong> 
		</div>

		<div class="alert alert-dismissible alert-info col-md-4 data-dashboard--last">
		  <strong>Ventas totales: ${{number_format($sells_total_money, 2)}}</strong> 
		</div>
</div>

<div class="row">
	<div class="alert alert-dismissible alert-info">
	  <strong>Productos vendidos en total</strong> 
	</div>
	<div id="products_sell"></div>
	@piechart('Vendidos', 'products_sell')
</div>

<div class="row">
	<div class="alert alert-dismissible alert-info">
	  <strong>Total de ventas mensuales</strong> 
	</div>
	<div id="sells_month"></div>
	@columnchart('Ventas', 'sells_month')
</div>	
	
@endsection