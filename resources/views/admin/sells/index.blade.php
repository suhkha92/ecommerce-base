@extends('layouts.inner--layout-admin')
@section('title-section-admin')Ventas @stop

@section('content-admin')
	@if (session('success'))
		<div class="alert alert-dismissible alert-info">
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		  {{ session('success') }}
		</div>
	@endif
	<div class="table-responsive">
		<table class="table table-responsive table-striped table-hover" id="myTable">
			<thead>
				<tr>
					<th>#</th>
					<th>Cliente</th>
					<th>Total</th>
					<th>Fecha</th>
					<th>Estatus</th>
					<th>Guía</th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>

			@if(count($sells) > 0)
				@foreach($sells as $sell)
					<tr>
						<td>{{$sell->id}}</td>
						<td>{{$sell->name}}</td>
						<td>${{number_format($sell->total, 2)}}</td>
						<td>{{$sell->created_at}}</td>
						<td>
							@php
								switch ($sell->status) {
									case '1':
										$status = "Pendiente";
										break;
									case '2':
										$status = "Pagado";
										break;
									case '3':
										$status = "En proceso";
										break;
									case '4':
										$status = "Enviado";
										break;
								}
							@endphp
							{{$status}}
						</td>
						<td>
							@if($sell->track_code == '')
								No hay guía aún
							@else
								{{$sell->track_code}}
							@endif
						</td>
						<td>
						@if($sell->track_code == '')
							No hay guía aún
						@else
							@if($sell->email_status == 0)	
								<a href="{{url('/admin/sells/sendtrack/'.$sell->user_id)}}">Envíar guía</a>
							@else
								<a href="{{url('/admin/sells/sendtrack/'.$sell->user_id)}}">Reenviar guía</a>
							@endif
						@endif
						</td>
						<td>
							<a href="{{url('/admin/sells/edit/'.$sell->id)}}">Editar</a>
						</td>
						<td><a href="{{url('/admin/sells/show/'.$sell->id)}}">Detalle de venta</a></td>
					</tr>
				@endforeach
			@else
				No hay resultados
			@endif
			</tbody>
		</table>
	</div>
@endsection