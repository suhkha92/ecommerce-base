@extends('layouts.inner--layout-admin')
@section('title-section-admin')Detalle de venta <a href="javascript:history.back()" class="right"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Regresar</a>@stop

@section('content-admin')
	<div class="table-responsive">
		<table class="table table-responsive table-striped table-hover">
			<thead>
				<th>ID</th>
				<th>ID de venta</th>
				<th>Producto</th>
				<th>Costo unitario</th>
				<th>Cantidad</th>
				<th>Fecha de pedido</th>
			</thead>
			<tbody>
			@foreach($sellsArr as $sell)
				<tr>
					<td>{{ $sell['id'] }}</td>
					<td>{{ $sell['sell_id'] }}</td>
					<td>
						<a href="{{url('/admin/products/show/'.$sell['product_id'])}}">
							{{ $sell['product'] }}
						</a>
					</td>
					<td>${{number_format($sell['price'], 2)}}</td>
					<td>{{ $sell['quantity'] }}</td>
					<td>{{ $sell['created_at'] }}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
@endsection