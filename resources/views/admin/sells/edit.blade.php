@extends('layouts.inner--layout-admin')
@section('title-section-admin')Editar pedido <a href="javascript:history.back()" class="right"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Regresar</a>@stop

@section('content-admin')
	
	<form class="form-horizontal" method="post" action="{{ url('admin/sells/update/'.$sell->id) }}">
		{{ csrf_field() }}
		<fieldset>
			<div class="form-group">
				<label for="inputTrackCode" class="col-lg-2 col-lg-offset-2 control-label">Guía de envío</label>
				<div class="col-lg-6">
					<input type="text" class="form-control" id="inputTrackCode" name="track_code" value="{{$sell->track_code}}" placeholder="Guía de envío">
					@if ($errors->has('track_code'))
					    <span class="help-block">
					        <strong>{{ $errors->first('track_code') }}</strong>
					    </span>
					@endif
				</div>
				<div class="form-group">
					<label for="" class="col-lg-2 col-lg-offset-2 control-label">Estatus de venta</label>
					<div class="col-lg-3">
						<input type="radio" name="status" class="form-control input-radio" value="1" {{ $sell->status == '1' ? 'checked' : '' }}> Pendiente
						<input type="radio" name="status" class="form-control input-radio" value="2" {{ $sell->status == '2' ? 'checked' : '' }}> Pagado
					</div>
					<div class="col-lg-3">
						<input type="radio" name="status" class="form-control input-radio" value="3" {{ $sell->status == '3' ? 'checked' : '' }}> En proceso
						<input type="radio" name="status" class="form-control input-radio" value="4" {{ $sell->status == '4' ? 'checked' : '' }}> Enviado
					</div>
				</div>
			</div>
			<button type="submit" class="btn btn-default col-lg-offset-11">Guardar</button>
		</fieldset>
	</form>
	
@endsection