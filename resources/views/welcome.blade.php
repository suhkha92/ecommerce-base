<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <title>Ecommerce</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:type"   content="website" /> 
    <meta property="og:url"    content="" /> 
    <meta property="og:title"  content="Ecommerce" /> 
    <meta property="og:image"  content="{{asset('images/cover.png')}}" /> 
    <meta name="description" content="">

    <link rel="shortcut icon" href="{{asset('/favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('/favicon.ico')}}" type="image/x-icon">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Rochester" rel="stylesheet">
    <link href="{{ asset('css/landing.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vendor.min.css') }}" rel="stylesheet">

</head>

<body>
  <main class="container card">
    <div class="card__right"></div>
    <div class="card__left">
      <div class="card__info">
        <img src="{{asset('images/cover.png')}}" class="img-responsive center-block" alt="">
        <h1 class="card__title">Lorem ipsum dolor sit amet,<span class="underline">consectetur</span> adipisicing elit</h1>
        <p class="card__description">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim culpa debitis animi necessitatibus exercitationem omnis ratione voluptate ullam, asperiores provident ab iste soluta nostrum magnam nesciunt qui. Ipsa, quo, earum!
        </p>
    </div>

    <div class="card__social_media">
      <a href="#" target="_blank" class="card__login card__login--facebook">
          <span>Síguenos en Facebook</span>
      </a>

      <a href="#" target="_blank" class="card__login card__login--twitter">
          <!--check this case-->
          <span>Síguenos en Twitter</span>
      </a>

      <div class="card__footer">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <p class="card__recommendation">Enim culpa debitis animi necessitatibus exercitationem omnis ratione voluptate ullam <a href="#" class="card__link_login_email">mail@mail.com</a></p>
            </div>
          </div>
      </div>
</div>
<!--end div card__social_media-->
</div>
<!--end div card__left-->
</main>
<script src="https://use.fontawesome.com/be7e763425.js"></script>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-107201065-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());

  gtag('config', 'UA-107201065-1');
</script>

</body>

</html>
